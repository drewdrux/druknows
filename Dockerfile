FROM ubuntu:16.04
USER root
RUN apt update && \
    apt -y install wget python python-dev python-pip python-virtualenv

RUN mkdir /code
WORKDIR /code
ADD . /code/
ADD requirements.txt /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install wheel
RUN python manage.py createcachetable cache_table
RUN python manage.py migrate
