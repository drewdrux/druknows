"""
WSGI config for druKnows project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application
import sys
import site

# Add the app's directory to the PYTHONPATH
sys.path.append('/code/')
sys.path.append('/code/druKnows/')
sys.path.append('/code/client/')
sys.path.append('/code/clips/')
sys.path.append('/code/data/')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "druKnows.settings")

application = get_wsgi_application()
